       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Listing12-4.
       AUTHOR. TEERARAK.

       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  PersentToApply    PIC 9(3).
       01  Percentage REDEFINES PersentToApply PIC 9v99.

       01  BaseAmount        PIC 9(5) VALUE 10555.
       01  PercentOfBase     PIC ZZ,ZZ9.99.
       01  PrnPercent        PIC ZZ9.

       PROCEDURE DIVISION .
       Begin.
           MOVE  23 TO PersentToApply
           COMPUTE PercentOfBase = BaseAmount * Percentage 
           DISPLAY "23% of 10555 is = " PercentOfBase
           MOVE  PersentToApply TO Percentage 
           DISPLAY "Percentage applied was " PrnPercent "%"
           STOP RUN.


